<?php
function digits($num){
    while ($num > 10){
        yield ($num%10);
        $num = floor($num/10);
    }
    yield ($num);
}

$inum = (int)readline("Please enter the number to check: ");
foreach (digits($inum) as $x){
    $inum-=$x**3;
}
if ($inum==0){
    print ("This is an armstrong number");
}
else {
    print  ("This is not an armstrong number");
}
?>
