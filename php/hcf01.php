<?php
$num01 = (int)readline("Enter the first number: ");
$num02 = (int)readline("Enter the second number: ");

$hcf = $num01; $mod = $num02;
while ($mod != 0){
    list($hcf, $mod) = array($mod, $hcf%$mod);
}
print ("The HCF of the numbers is $hcf and LCM is ".$num01*$num02/$hcf);
