<?php
$num = (int)readline("Enter number to be factorized: ");
$factors = [];
for ($idx=1; $idx<floor($num**0.5); $idx++){
    if ($num%$idx==0){
        array_push($factors,$idx, $num/$idx);
    }
}
if (($idx)**2 == $num){
        array_push($factors,$idx);
}
sort($factors);
print("The factors of the number are: ".implode(", ",$factors));
?>
