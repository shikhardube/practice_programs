num_list = [26, 2, 7, 8 ,0,1 ,8 ,90 ,0] # using num_list as list is a built in class
num_min = num_list[0]                   # using num_min as min is a built in function
for num in num_list:                    # iterating over the list directly
    if num < num_min:
        num_min = num
print ("The minimum no. is",num_min)
print ("It occurs at positions:",end=" ") # how to print on the same line
for j in range(len(num_list)):
    if num_list[j]==num_min:
        print (j,end=" ")
