def digits(num):
    while num >= 10:
        yield num % 10
        num //= 10
    else: yield num

inum = int(input("Please enter the number to check: "))
for x in digits(inum): inum-=x**3
if (inum==0):
    print ("This is an armstrong number")
else:
    print  ("This is not an armstrong number")
