from transpose01 import transpose

def mtx_product(matA, matB):
    if len(matA[0]) != len(matB): return ["Error"]
    return [[sum(x*y for x,y in zip(row,col)) for col in transpose(matB)] for row in matA]

if __name__ == "__main__":
    matA = [[1,2,3],[4,5,6],[7,8,9]]
    matB = [[9,2,8,2],[3,1,8,5],[5,1,7,6]]
    print ("The product of the matrix is:")
    for row in mtx_product(matA, matB):
        print (*row, sep="\t")
