from __future__ import print_function
import sys, os, time

x = [[1,4,5],
    [0 ,2,8],
    [6 ,9,3]]
    
xt = [[0,0,0],
      [0,0,0],
      [0,0,0]]
         
a = len(x[0])         
for i in range(len(x)):    # iterate through rows
   for j in range(a):     # iterate through columns
       xt[j][i] = x[i][j]

for elements in xt:
   print(elements)
   