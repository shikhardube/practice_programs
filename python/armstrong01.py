def digits(num):
    if num < 10: return ([num])
    else: return (digits(num//10)+[num%10])

if __name__ == "__main__":
    inum = int(input("Please enter the number to check: "))
    for x in digits(inum): inum-=x**3
    if (inum==0): print ("This is an armstrong number")
    else:         print  ("This is not an armstrong number")
