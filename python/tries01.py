for tries in range(5):
    try: num = int(input("Enter a number: "))
    except: print ("Not a valid number.", end=" ")
    else:
        print("The closest multiple of 5 to the number is", ((num*2+5)//10)*5)
        break
else:
    print ("Terminating, too many wrong inputs")
