def transpose(matrix):
    return [[row[col_no] for row in matrix] for col_no in range(len(matrix[0]))]

if __name__ == "__main__":
    matrix = [ [1,25],[2,40],[4,38] ]
    for row in transpose(matrix):
        print (*row, sep="\t")
