num01 = int(input("Enter the first number: "))
num02 = int(input("Enter the second number: "))

hcf,mod = num01,num02
while mod != 0:
    hcf, mod = mod, hcf%mod

print ("The HCF of the numbers is %d and LCM is %d"%(hcf,num01*num02/hcf))
