### alternative approach to the problem looping over the list only once ###
num_list = [26, 2, 7, 8, 0, 1, 8, 90, 0] # using num_list as list is a built in class
num_min = num_list[0]                   # using num_min as min is a built in function
min_pos = [0]                           # using min_pos array to store where min occurs
for inx, num in enumerate(num_list):
    if num < num_min:
        num_min = num
        min_pos = [inx]
    elif num == num_min:
        min_pos += [inx]
print ("The minimum no. is",num_min)
print ("It occurs at positions:",min_pos)
