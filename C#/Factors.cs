using System;
using System.Collections.Generic;
public class Factors
{
    public static void Main()
    {
        Console.Write("Enter the number whose factors are to be found: ");
        int number = Convert.ToInt32(Console.ReadLine()) ;

        List<int> factors = new List<int>();
        int divisor ;
        for (divisor=1; divisor < Math.Sqrt(number); divisor++){
            if (number%divisor == 0){
                factors.Insert(factors.Count/2, number/divisor); //insert the new found factors in the middle of the list
                factors.Insert(factors.Count/2, divisor);
            }
        }
        if (divisor == Math.Sqrt(number)) factors.Insert(factors.Count/2, divisor); // If a perfect square add its root

        // print all factors //
        Console.Write("The factors of the number are: ");
        foreach (int factor in factors){
            Console.Write("{0} ",factor);
        }
    }
}
