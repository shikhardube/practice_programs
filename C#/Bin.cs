using System;
public class Fact
{
    public static void Main()
    {
        Console.Write("The Number whose binary equivalent is to be calculated: ");
        int num = int.Parse(Console.ReadLine());
        string binary = "";
        do {
            binary = (num%2).ToString()+binary;
            num /= 2;
        } while (num > 0);
        Console.WriteLine ("Binary Equivalent of the number is {0}", binary);
    }
}
