using System;
public class TryNum
{
    public static void Main()
    {
        Console.Write("Enter an integer: ");
        // Read Number
        try{
            int num = Convert.ToInt32(Console.ReadLine()) ;
            num = (int)Math.Round((decimal)num/10)*10; // Find closest multiple of 10
            Console.WriteLine("The number {0}", num);
        }
        catch (FormatException){
            Console.WriteLine("Not an integer");
        }

    }
}
