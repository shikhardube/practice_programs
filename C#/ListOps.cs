using System;
using System.Collections.Generic;
public class ListOps
{
    public static void Main()
    {
        List<string> fruits = new List<string>();
        addFruits(fruits);
        Console.WriteLine("Picking the first five fruits in a hurry: ");
        for (int i=0;i<5;i++){
            Console.WriteLine(fruits[i]);
        }
        Console.WriteLine();
        Console.WriteLine("Picking every alternate fruit: ");
        for (int i=0;i<fruits.Count;i+=2){
            Console.WriteLine(fruits[i]);
        }
        Console.WriteLine();
        Console.WriteLine("Now picking them in reverse order: ");
        fruits.Reverse();
        for (int i=0;i<fruits.Count;i++){
            Console.WriteLine(fruits[i]);
        }


    }

    public static void addFruits(List<string> fruits){
        fruits.Add("Apple");
        fruits.Add("Banana");
        fruits.Add("Cherry");
        fruits.Add("Pear");
        fruits.Add("Kivi");
        fruits.Add("Orange");
        fruits.Add("Fig");
        fruits.Add("Guava");
    }
}
