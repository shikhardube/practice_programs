using System;
public class EvenOdd
{
    public static void Main()
    {
        Console.Write("Enter a number: ");
        // Read Number
        int num = Convert.ToInt32(Console.ReadLine()) ;
        string result = num%2 == 0 ? "Even" : "Odd" ;
        // Print formatted output
        Console.WriteLine("The number {0} is {1}", num, result);
    }
}
