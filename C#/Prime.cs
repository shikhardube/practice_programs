using System;
public class Prime
{
    static int[] prime = new int[1000];

    public static void Main()
    {
        prime[0]=2 ; //first prime is 2//
        int idx=1, number = 3;
        // loop over every consecutive odd number till prime array is full (idx = array length)//
        while (idx < prime.Length){
            if (isPrime(number)){ // if a number is prime add it to the array and increase idx by one //
                prime[idx]=number;
                idx++;
            }
            number += 2;
        }

        // print all prime nos
        Console.WriteLine("The first 1000 prime number are:");
        foreach (int num in prime){
            Console.Write("{0} ",num);
        }
    }

    static bool isPrime(int num){
        int divIndex = 0 ;
        // check divisibility with all prime divisors till the sqrt of the num //
        while (prime[divIndex]*prime[divIndex] <= num) {
            if (num%prime[divIndex]==0){
                return (false);
            }
            divIndex++;
        }
        return (true);
    }
}
