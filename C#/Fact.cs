using System;
public class Fact
{
    public static void Main()
    {
        Console.Write("The Number whose factorial is to be calculated: ");
        int num = int.Parse(Console.ReadLine());
        double factorial = 1;

        for (int i=1; i<=num; i++){
            factorial *= i ;
        }
        Console.WriteLine ("Factorial of {0} is {1}", num, factorial);
    }
}
