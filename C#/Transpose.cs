using System;
namespace Matrix{

    public class Transpose
    {
        public static void Main()
        {
            int [,] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
            Console.WriteLine("The transposed matrix is:");
            ShowMat(transpose(matrix));
        }

        public static void ShowMat(int [,] mat){
            for (int row=0; row < mat.GetLength(0) ; row++){
                for (int col=0; col < mat.GetLength(1); col++) {
                    Console.Write("{0} ", mat[row, col]);
                }
                Console.WriteLine();
            }
        }

        public static int[,] transpose (int[,] matrix){
            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);

            int [,] transpose = new int[cols,rows];
            for (int row=0; row < rows ; row++){
                for (int col=0; col < cols; col++) {
                    transpose [col,row] = matrix [row,col] ;
                }
            }
            return (transpose);
        }
    }
}
