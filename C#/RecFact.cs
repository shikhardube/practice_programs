using System;
public class RecFact
{
    public static void Main()
    {
        Console.Write("The Number whose factorial is to be calculated: ");
        int num = int.Parse(Console.ReadLine());
        Console.WriteLine ("Factorial of {0} is {1}", num, factorial(num));
    }

    static double factorial(int num){
        if (num <= 1) return (1);
        else return (num * factorial(num-1));
    }
}
