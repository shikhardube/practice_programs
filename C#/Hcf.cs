using System;
public class Hcf
{
    public static void Main()
    {
        Console.Write("Enter first number: ");
        int num1 = Convert.ToInt32(Console.ReadLine()) ;

        Console.Write("Enter second number: ");
        int num2 = Convert.ToInt32(Console.ReadLine()) ;

        int dividend = num1, divisor = num2, remainder = dividend%divisor;
        while (remainder !=0){
            dividend = divisor;
            divisor = remainder;
            remainder = dividend%divisor ;
        }

        // Print formatted output
        Console.WriteLine("The HCF of number {0} & {1} is {2}", num1,num2,divisor);
    }
}
