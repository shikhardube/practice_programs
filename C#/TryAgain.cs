using System;
public class TryAgain
{
    public static void Main()
    {
        int tries = 0;
        do{
            try{
                Console.Write("Enter an integer: ");
                int num = Convert.ToInt32(Console.ReadLine()) ;
                num = (int)Math.Round((decimal)(2*num)/10)*5; // Find closest multiple of 5
                Console.WriteLine("The number {0}", num);
                break;
            }
            catch (FormatException){
                Console.WriteLine("Not an integer, try again.");
                tries++;
            }
        } while(tries<5);

        if (tries >= 5) Console.WriteLine("Oh No!! of tries exceeded, exiting program.");
    }
}
