using System;
public class RecBin
{
    public static void Main()
    {
        Console.Write("The Number whose binary equivalent is to be calculated: ");
        int num = int.Parse(Console.ReadLine());
        Console.WriteLine ("Binary Equivalent of {0} is {1}", num, BinEq(num));
    }

    static string BinEq(int num){
        if (num <= 1) return (num.ToString());
        else return (BinEq(num/2)+(num%2).ToString());
    }
}
