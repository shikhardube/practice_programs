using System;
public class FindSeries
{
    public static void Main()
    {
        Console.Write("Enter first term: ");
        int term_1 = Convert.ToInt32(Console.ReadLine()) ;

        Console.Write("Enter second term: ");
        int term_2 = Convert.ToInt32(Console.ReadLine()) ;

        Console.Write("Enter third term: ");
        int term_3 = Convert.ToInt32(Console.ReadLine()) ;

        if (term_2-term_1 == term_3-term_2) Console.WriteLine("The series is an AP Series");
        else if (term_2/term_1 == term_3/term_2) Console.WriteLine("The series is an GP Series");
        else Console.WriteLine("The series is neither AP nor GP");
    }
}
