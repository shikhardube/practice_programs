using System;
public class Min
{
    public static void Main()
    {
        int[] numbers = {26, 2, 0, 8, 0, 10, 8, 90, 5} ;
        int min ;
        min = numbers[0];
        foreach (int num in numbers) {
            if (num < min) min = num;
        }
        Console.Write("Minimun number {0} ", min);
        Console.Write("occurs at index ");
        for (int i=0; i<numbers.Length; i++){
            if (numbers[i] == min) Console.Write("#{0} ",i);
        }
    }
}
