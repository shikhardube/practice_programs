using System;
public class Fact
{
    public static void Main()
    {
        Console.Write("The Number whose binary equivalent is to be calculated: ");
        int number = int.Parse(Console.ReadLine());
        string oct = "", hex ="", hexSymbols="0123456789ABCDEF" ;

        int num = number ;  // copy number as it will undergo change on division //
        do { // using do loop for case num = 0 //
            oct = (num%8).ToString()+oct;
            num /= 8;
        } while (num > 0);
        Console.WriteLine ("Octal Equivalent of the number is {0}", oct);

        num = number ;      // copy number as it will undergo change on division //
        do {
            hex = (hexSymbols[num%16]).ToString()+hex;
            num /= 16;
        } while (num > 0);
        Console.WriteLine ("Hex Equivalent of the number is {0}", hex);
    }
}
