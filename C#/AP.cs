using System;
public class AP
{
    public static void Main()
    {
        Console.Write("Enter first term: ");
        int a = Convert.ToInt32(Console.ReadLine()) ;

        Console.Write("Enter common difference: ");
        int d = Convert.ToInt32(Console.ReadLine()) ;

        Console.Write("Enter desired no of terms: ");
        int n = Convert.ToInt32(Console.ReadLine()) ;

        Console.Write("The series is: ");
        int a_n = a, sum = a_n;
        for(int i=1; i<n; i++){
            Console.Write("{0}, ", a_n);
            a_n += d;
            sum += a_n;
        }

        Console.WriteLine("{0}", a_n);
        Console.WriteLine("The sum of the series is {0}",sum);
    }
}
