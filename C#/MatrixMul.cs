// Using functions written in Transpose in this file
// use csc /t:library /out:Transpose.dll Traspose.cs
// to compile Transpose to Transpose.dll for use by this program.
// Then use csc /reference:Transpose.dll /t:exe /out:MatrixMul.exe MatrixMul.cs
// to compile this file with Transpose dependency for proper functioning of the program.
using System;
namespace Matrix {
    public class MatrixMul
    {
        public static void Main()
        {
            int [,] MatA = {{1,2,3},{5,6,7},{9,10,11}};
            int [,] MatB = {{1,2,3},{5,6,7}};
            int [,] result ;

            if (MatA.GetLength(1) == MatB.GetLength(0)){ //if colof first = rows of second//
                result = matrixmul(MatA, MatB);
            }
            else if(MatA.GetLength(1) == MatB.GetLength(1)){ //checking if multiplication possible with transpose//
                Console.WriteLine("Executing multiplication with transpose of second matrix");
                result = matrixmul(MatA, Transpose.transpose(MatB));
            }
            else {
                Console.WriteLine("No proper dimentions found for matrix multiplication");
                return ;
            }

            Console.WriteLine("The resultant matrix is:");
            Transpose.ShowMat(result);
        }

        public static int [,] matrixmul(int [,] matA, int [,] matB){
            int rowA = matA.GetLength(0);
            int colB = matB.GetLength(1);
            int comAB = matA.GetLength(1);

            int [,] result = new int [rowA,colB];
            // multiply matrix //
            for (int row=0; row < rowA; row++){
                for (int col=0; col < colB; col++){
                    for (int i=0; i < comAB; i++) {
                        result[row,col] += matA[row,i]*matB[i,col] ;
                    }
                }
            }
            return (result);
        }
    }
}
