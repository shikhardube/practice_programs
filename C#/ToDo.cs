using System;
using System.Collections.Generic;

public class ToDo
{
    public static void Main()
    {
        List<Task> tasks = new List<Task>();
        int op;

        do{
            Console.WriteLine("Choose operation:");
            Console.WriteLine("1. Add Task");
            Console.WriteLine("2. Mark task completed");
            Console.WriteLine("3. Reorder Task");
            Console.WriteLine("4. Remove all completed tasks");
            Console.WriteLine("5. Exit");
            op = int.Parse(Console.ReadLine());
            switch(op){
                case 1: // Add Task //
                    Console.WriteLine("Enter task");
                    string newTask = Console.ReadLine();
                    tasks.Add(new Task(newTask));
                    break ;
                case 2: // Mark Completed //
                    Console.WriteLine("Choose task-id from list:");
                    int taskId = int.Parse(Console.ReadLine());
                    tasks[taskId-1].MarkCompleted();
                    break ;
                case 3: // Reorder //
                    Console.WriteLine("Choose task-id from list:");
                    int curId = int.Parse(Console.ReadLine());
                    Console.WriteLine("At which index would you like to move it:");
                    int newId = int.Parse(Console.ReadLine());
                    Task task = tasks[curId-1]; // create a ref to the task
                    tasks.RemoveAt(curId-1);    // remove from list
                    tasks.Insert(newId-1,task); // copy ref to new position in list
                    break ;
                case 4: // Remove all completed //
                    tasks.RemoveAll(x=>x.Completed); // using lambda fn as predicate //
                    break ;
                case 5:
                    Console.WriteLine("Exiting Program");
                    break ;
                default:
                    Console.WriteLine("Incorrect Input. Please choose a number");
                    break ;
            }
            showList(tasks);
            Console.WriteLine();
        } while(op != 5);

        // The operation you want to do

        // Reorder task
        // Mark item completed
        // Remove all completed tasks

    }

    public static void showList(List<Task> tasks)
    {
        for (int i = 0; i < tasks.Count; i++){
            Console.WriteLine("{0}: {1}", i+1, tasks[i]);
        }
    }
}

public class Task
{
    private string Name {get; }
    private bool completed ;

    public bool Completed { // set read only external property for use by remove all
        get { return completed;}
    }

    public Task(string task)
    {
        Name = task;
        completed = false;
    }

    public void MarkCompleted()
    {
        completed = true ;
    }

    public override string ToString()
    {
        return (Name+":"+(completed ? "Completed" : "Incomplete"));
    }
}
