using System;
public class Fib
{
    public static void Main()
    {
        Console.Write("How many terms of the series are desired: ");
        // Read Number
        int numTerms = Convert.ToInt32(Console.ReadLine()) ;
        double n = 0, nxt = 1;
        for (int i=1; i<numTerms; i++){
            Console.Write("{0}, ",n);
            // n= nxt & nxt = sum of current elements
            nxt += n; n = nxt - n;
        }
        Console.Write(n);
    }
}
